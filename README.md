## Exercice API : Rick & Morty

Une API existe contenant des données sur les personnages de la série connue **Rick and Morty**.
- [https://rickandmortyapi.com/api](https://rickandmortyapi.com/api)
- [Documentation de l'api](https://rickandmortyapi.com/documentation/#rest)
 
Consommez cette API avec la technologie de votre choix.
Au sein de l'application ainsi implémentée votre API devra a minima: 
- Lister l'ensemble des personnages
- Avoir une fonctionnalité de pagination pour voir les différents personnages
- Voir la fiche d'un personnage

Voici un exemple simple permettant de requêter l'API
```javascript
fetch("https://rickandmortyapi.com/api/character")
.then(res => res.json())
.then(o => o.results)
.then(console.log)
.catch(console.error)
```

Une suggestion est d'utiliser ou de découvrir un framework js. Pourquoi pas nestJS : 

Installation de nestJS.
https://docs.nestjs.com/first-steps

Il vous suffit de rajouter :

```shell
git clone https://github.com/nestjs/javascript-starter.git nestjs-rick-and-morty
cd nestjs-rick-and-morty
npm i
npm run start:dev
```


```javascript
// app.service.js
async getCharacters() {
return await fetch("https://rickandmortyapi.com/api/character")
.then(res => res.json())
.then(o => o.results)
}
```
et
```javascript
//app.controller.js
@Get("char")
getCharacters() {
return this.appService.getCharacters();
}
```

Et vous voilà avec un serveur web retournant des données de l'API. 
Il ne reste plus qu'à lui associer un modèle mvc
https://docs.nestjs.com/techniques/mvc#template-rendering

Bon courage !